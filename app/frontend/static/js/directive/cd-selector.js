define([
    './module.js',
    'jquery'
],
    function (directives,$) {
        directives
            .directive('cdSelector',['$document','$compile','$rootScope','$http',function($document,$compile,$rootScope,$http){
                var body = $document.find('body'),
                    options = {
                        templates: {
                            layout:'/templates/geo_selector/layout.html',
                            country:'/templates/geo_selector/country.html',
                            area:'/templates/geo_selector/area.html',
                            town:'/templates/geo_selector/town.html',
                            street:'/templates/geo_selector/street.html'
                        },
                        api: {
                            country: $rootScope.server.dashboard+'/api/dashboard/get/all/countries/',
                            area: $rootScope.server.dashboard+'/api/dashboard/get/all/areas/',
                            town: $rootScope.server.dashboard+'/api/dashboard/get/all/towns/',
                            street: $rootScope.server.dashboard+'/api/dashboard/get/all/streets/'
                        }
                    },
                    selectorLayout,layoutCountry,layoutArea,layoutTown,layoutStreet;

                if(!$rootScope.geo_selector)
                    $rootScope.geo_selector = {};

                $rootScope.geo_selector.set = function(name,value) {
                    $rootScope.geo_selector[name] = value;
                }

                $rootScope.geo_selector.get = function(name) {
                    if($rootScope.geo_selector[name])
                        return $rootScope.geo_selector[name];
                }

                function getMainSymbol(string) {
                    var regExp = /([А-Я])+/g;
                    var result = string.match(regExp);
                    if(result) {
                        return result[0];
                    } else {
                        return string[0];
                    }
                }

                function splitByAlphabet(_array) {
                    var rusAlphabet = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','щ','ш','ь','ы','ъ','э','ю','я'];
                    var result = [], group_blocks = [], group_block = [];

                    for(var i = 0, objectLength = _array.length;i < objectLength; i++) {
                        if(_array[i].name) {
                            var index = rusAlphabet.indexOf(getMainSymbol(_array[i].name).toLowerCase());
                            addInLetterGroup(result,rusAlphabet[index],_array[i]);
                        }
                    }
                    for(var i = 0, resultLength = result.length; i < resultLength; i++) {
                        if(i%3 == 0 && i != 0) {
                            group_blocks.push(group_block);
                            group_block = [];
                        }
                        group_block.push(result[i]);
                    }
                    group_blocks.push(group_block);

                    return group_blocks;
                }

                function addInLetterGroup(group_container,letter, item) {
                    if(group_container.length > 0) {
                        for(index in group_container) {
                            if(group_container[index].letter == letter) {
                                if(group_container[index].content.length > 0) {
                                    group_container[index].content.push(item);
                                } else {
                                    group_container[index].content = [item];
                                }
                                return;
                            }
                        }
                    }
                    group_container.push({letter: letter, content: [item]})
                }

                function removeBase(scope) {
                    if(scope.country_base)
                        delete(scope.country_base);
                    if(scope.area_base)
                        delete(scope.area_base);
                    if(scope.town_base)
                        delete(scope.town_base);
                    if(scope.street_base)
                        delete(scope.street_base);

                    if(layoutArea)
                        layoutArea.remove();
                    layoutArea = null;

                    if(layoutCountry)
                        layoutCountry.remove();
                    layoutCountry = null;

                    if(layoutTown)
                        layoutTown.remove();
                    layoutTown = null;

                    if(layoutStreet)
                        layoutStreet.remove();
                    layoutStreet = null;
                }

                function removeAll(scope) {
                    removeBase(scope);

                    if(selectorLayout)
                        selectorLayout.remove();

                    selectorLayout = null;
                    body.removeClass('no-scroll');
                }

                function loadLayout(scope) {
                    body.addClass('no-scroll');
                    selectorLayout = angular.element('<div class="b-geo-selector"><div class="b-geo-selector__bg"></div></div>');
                    body.append(selectorLayout);

                    selectorLayout.find('.b-geo-selector__bg').click(function(){
                        removeAll(scope);
                    })
                }

                function loadStreet(scope) {
                    if(!selectorLayout)
                        loadLayout(scope);

                    setTimeout(function(){
                        layoutStreet = angular.element('<div ng-include="\'' + options.templates.street + '\'"></div>');
                        var element = $compile(layoutStreet)(scope);

                        selectorLayout.append(layoutStreet);

                        scope.street_loading = true;

                        $http.get(options.api.street+scope.town._id+'/')
                            .success(function(res){
                                scope.street_base = splitByAlphabet(res);
                                scope.street_loading = false;
                            })
                            .error($rootScope.handleRequestError);

                        scope.setStreet = function(street) {
                            scope.street = street;
                            removeAll(scope);
                            $rootScope.geo_selector.set('street',street);
                        }
                    },200);
                }

                function loadTown(scope,onlyTown) {
                    if(!selectorLayout)
                        loadLayout(scope);

                    setTimeout(function(){
                        layoutTown = angular.element('<div ng-include="\'' + options.templates.town + '\'"></div>');
                        var element = $compile(layoutTown)(scope);

                        selectorLayout.append(layoutTown);

                        scope.town_loading = true;

                        $http.get(options.api.town+scope.area._id+'/')
                            .success(function(res){
                                scope.town_base = splitByAlphabet(res);
                                scope.town_loading = false;
                            })
                            .error($rootScope.handleRequestError);

                        scope.setTown = function(town) {
                            scope.town = town;

                            if(!onlyTown) {
                                removeBase(scope);
                                loadStreet(scope);
                            } else
                                removeAll(scope);
                            $rootScope.geo_selector.set('town',town);
                        }
                    },200);
                }

                function loadArea(scope,onlyArea,onlyTown) {
                    if(!selectorLayout)
                        loadLayout(scope);

                    setTimeout(function(){
                        layoutArea = angular.element('<div ng-include="\'' + options.templates.area + '\'"></div>');
                        var element = $compile(layoutArea)(scope);

                        selectorLayout.append(layoutArea);

                        scope.area_loading = true;

                        $http.get(options.api.area+scope.country._id+'/')
                            .success(function(res){
                                scope.area_base = splitByAlphabet(res);
                                scope.area_loading = false;
                            })
                            .error($rootScope.handleRequestError);

                        /**
                         * Срабатывает при клике по пункту в области выбора
                         *
                         * @param area
                         */
                        scope.setArea = function(area) {
                            scope.area = area;

                            /**
                             * Если нужен город то загружаем список городов, иначе удаляем гео контрол
                             */
                            if(!onlyArea) {
                                scope.town = {};
                                removeBase(scope);

                                loadTown(scope,onlyTown);
                            } else
                                removeAll(scope);
                            $rootScope.geo_selector.set('area',area);
                        }
                    },200);
                }

                /**
                 * Отображаем список стран для выбора, если требуется после выбора страны выбрать область или город то
                 * в соответсвующие параметры передаём true
                 *
                 * @param scope
                 * @param onlyCountry
                 * @param onlyArea
                 * @param onlyTown
                 */
                function loadCountry(scope,onlyCountry,onlyArea,onlyTown) {
                    if(!selectorLayout)
                        loadLayout(scope);

                    setTimeout(function(){
                        var layoutCountry = angular.element('<div ng-include="\'' + options.templates.country + '\'"></div>'),
                            element = $compile(layoutCountry)(scope);

                        selectorLayout.append(layoutCountry);

                        scope.country_loading = true;

                        $http.get(options.api.country)
                            .success(function(res){
                                scope.country_base = res;
                                scope.country_loading = false;
                            })
                            .error($rootScope.handleRequestError);

                        scope.setCountry = function(country) {
                            scope.country = country;

                            if(!onlyCountry) {
                                removeBase(scope);
                                loadArea(scope,onlyArea,onlyTown);
                            } else
                                removeAll(scope);
                            $rootScope.geo_selector.set('country',country);
                        }
                    },200);
                }

                function link(scope,element, attrs) {
                    switch(attrs.cdSelector) {
                        case 'country':
                            scope.street = $rootScope.geo_selector.get('street');
                            element.on('click',function(e){
                                removeAll(scope);
                                if(element.hasClass('b-geo-selector_change_country')) {
                                    scope.street ? loadCountry(scope,false,false,false) : loadCountry(scope,false,false,true);
                                } else {
                                    loadCountry(scope,true);
                                }
                            });
                            scope.$watch('country',function(newValue, oldValue){
                                if(newValue)
                                    element.text(newValue.name);
                            });
                        break;
                        case 'area':
                            scope.country = $rootScope.geo_selector.get('country');
                            element.on('click',function(e){
                                removeAll(scope);
                                if(scope.country)
                                    element.hasClass('b-geo-selector_change_area') ? loadArea(scope,false,true) : loadArea(scope,true,false);
                                else
                                    element.hasClass('b-geo-selector_change_area') ? loadCountry(scope,false,false) : loadCountry(scope,false,true);
                            });
                            scope.$watch('area',function(newValue, oldValue){
                                if(newValue)
                                    element.text(newValue.name);
                            });
                        break;
                        case 'town':
                            scope.town = $rootScope.geo_selector.get('town');
                            element.on('click',function(e){
                                removeAll(scope);
                                if(!scope.area)
                                    if(scope.country) {
                                        element.hasClass('b-geo-selector_change_town') ? loadArea(scope,false,false) : loadArea(scope,false,true);
                                    } else {
                                        element.hasClass('b-geo-selector_change_town') ? loadCountry(scope,false,false,false) : loadCountry(scope,false,false,true);
                                    }
                                else
                                    loadTown(scope);
                            });
                            scope.$watch('town',function(newValue, oldValue){
                                if(newValue)
                                    element.text(newValue.name);
                            });
                        break;
                        case 'street':
                            scope.street = $rootScope.geo_selector.get('street');
                            element.on('click',function(e){
                                removeAll(scope);
                                if(!scope.town) {
                                    if(!scope.area)
                                        scope.country ? loadArea(scope,false,false) : loadCountry(scope,false,false,false);
                                    else {
                                        loadTown(scope);
                                    }
                                } else {
                                    loadStreet(scope);
                                }
                            });
                            scope.$watch('street',function(newValue, oldValue){
                                if(newValue)
                                    element.text(newValue.name);
                            });
                        break;
                    }
                }
                return {
                    link: link
                };
            }])
    }
);