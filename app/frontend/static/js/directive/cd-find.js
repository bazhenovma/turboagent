define([
    './module.js',
    'jquery',
    'underscore'
],
    function (directives,$,_) {

        'use strict';

        directives
            .directive('cdFind',['$document','$compile','$rootScope','$http','$filter',function($document,$compile,$rootScope,$http,$filter){

                var body = $document.find('body'), container, items, $scope, $element;

                function SetPropertyByString(scope,stringRepresentation,property) {
                    var properties = stringRepresentation.split("."),
                        obj = scope[properties[0]];

                    if(typeof obj == 'undefined') {
                        obj = {};
                    }

                    for (var i = 1, length = properties.length; i<length; i++) {
                        if(!_.isObject(obj[properties[i]])) {
                            obj[properties[i]] = property;
                        } else {
                            obj = scope[properties[i]];
                        }
                    }
                    scope[properties[0]] = obj;
                }

                function findAddressesByString(string,scope) {
                    var data = {_town: scope.town._id, text: string };

                    $http.post($rootScope.server.dashboard+'/api/dashboard/find/address/', $.param(data))
                        .success(function(response){
                            scope.auto_complete_items = response;
                        })
                        .error($rootScope.handleRequestError);
                }

                function buildRequestToKladr(query, emptyResultMessage,limit, callback) {
                    if(query.length < 3) {
                        delete($scope.auto_complete_error);
                        delete($scope.auto_complete_items);
                        return;
                    }
                    var default_url = 'http://kladr-api.ru/api.php?withParent=1&limit='+limit+'&token=536788cefca916e6390e2758&key=8daf624b1e0966b786f72b07149f18b2ef9dc1c6&callback=JSON_CALLBACK&'+query;
                    $scope.auto_complete_inprogress = true;
                    
                    $http.jsonp(default_url)
                        .success(callback ? callback : function(response){
                            if(response.result.length > 0) {
                                if($scope.auto_complete_error)
                                    delete($scope.auto_complete_error);
                                $scope.auto_complete_items = response.result;
                            } else {
                                delete($scope.auto_complete_items);
                                $scope.auto_complete_error = emptyResultMessage;
                            }
                            $scope.auto_complete_inprogress = false;
                        })
                        .error($rootScope.handleRequestError);
                }

                function findAreaByString(string,scope) {
                    buildRequestToKladr('query='+string+'&contentType=region','Такого региона не существует',2);
                }

                function findStreetByString(string,scope) {
                    if(scope.town)
                        buildRequestToKladr('query='+string+'&contentType=street&cityId='+scope.town.kladr_ID,'Такой улицы/проспекта не существует',2);
                }

                function findTownByString(string,scope) {
                    if(scope.area)
                        buildRequestToKladr('query='+string+'&contentType=city&regionId='+scope.area.kladr_ID,'Такого города не существует',2);
                }

                function appendAutoComplete(scope,element, onloadContainer) {
                    container = body.find('.b-auto-complete');

                    if(container.length == 0) {
                        container = angular.element('<div class="b-auto-complete" ng-show="auto_complete_items || auto_complete_error"><div onload="'+onloadContainer+'" ng-include="\'' + '/templates/auto-complete/index.html' + '\'"></div></div>');
                        $compile(container)(scope);
                        body.append(container);
                    }
                }

                function appendItems(url) {
                    items = container.find('.b-address');

                    if(items.length == 0) {
                        items = angular.element('<div ng-include="\'' + url + '\'"></div>');
                        $compile(items)($scope);
                        var content = container.find('.b-auto-complete__content');
                        content.append(items);
                    }
                }

                function appendStreetItems() {
                    appendItems('/templates/auto-complete/street.html');
                }

                function appendAddressItems() {
                    appendItems('/templates/auto-complete/addresses.html');
                }

                function appendAreaItems() {
                    appendItems('/templates/auto-complete/area.html');
                }

                function appendTownItems() {
                    appendItems('/templates/auto-complete/town.html');
                }

                function setAutoCompleteItem(item) {
                    SetPropertyByString($scope,$element.attr('ng-model'), item.name);
                    delete($scope.auto_complete_items);
                }

                function setAutoCompleteAddressItem(item) {
                    $scope.query.house._address = item;
                    delete($scope.auto_complete_items);
                }

                function setAutoCompleteAreaItem(item) {
                    SetPropertyByString($scope,$element.attr('ng-model'), item.name +' '+ item.type.toLowerCase());
                    delete($scope.auto_complete_items);
                }

                function cleanUp() {
                    container.remove();
                }

                function link(scope,element, attrs) {
                    $scope = scope;
                    scope.$on('$destroy', function() {
                        cleanUp();
                    });

                    switch(attrs.cdFind) {
                        case 'address':
                            scope.appendAddressItems = appendAddressItems;
                            scope.setAutoCompleteItem = setAutoCompleteAddressItem;
                            appendAutoComplete(scope,element,'appendAddressItems()');

                            element.on('keydown',function(e){
                                $element = element;
                                findAddressesByString(element.val(),scope);
                            });
                            element.on('focus',function(e){
                                container.css({
                                    top: element.offset().top+element.height()+'px',
                                    left: element.offset().left+'px'
                                });
                            });
                        break;
                        case 'area':
                            scope.appendAreaItems = appendAreaItems;
                            scope.setAutoCompleteAreaItem = setAutoCompleteAreaItem;
                            appendAutoComplete(scope,element,'appendAreaItems()');

                            element.on('keydown',function(e){
                                $element = element;
                                findAreaByString(element.val(),scope);
                            });
                            element.on('focus',function(e){
                                container.css({
                                    top: element.offset().top + element.height()+'px',
                                    left: element.offset().left+'px'
                                });
                            });
                        break;
                        case 'town':
                            scope.appendTownItems = appendTownItems;
                            scope.setAutoCompleteItem = setAutoCompleteItem;
                            appendAutoComplete(scope,element,'appendTownItems()');

                            element.on('keydown',function(e){
                                $element = element;
                                findTownByString(element.val(),scope);
                            });
                            element.on('focus',function(e){
                                container.css({
                                    top: element.offset().top + element.height()+'px',
                                    left: element.offset().left+'px'
                                });
                            });
                        break;
                        case 'street':
                            scope.appendStreetItems = appendStreetItems;
                            scope.setAutoCompleteItem = setAutoCompleteItem;
                            appendAutoComplete(scope,element,'appendStreetItems()');

                            element.on('keydown',function(e){
                                $element = element;
                                findStreetByString(element.val(),scope);
                            });
                            element.on('focus',function(e){
                                container.css({
                                    top: element.offset().top + element.height()+'px',
                                    left: element.offset().left+'px'
                                });
                            });
                        break;
                    }
                }
                return {
                    link: link
                };
            }])
    }
);