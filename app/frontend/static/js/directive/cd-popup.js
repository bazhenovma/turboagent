define([
    './module.js',
    'jquery',
    'ui'
],
    function (directives,$,ui) {
        'use strict';

        directives
            .directive('cdPopup',['$document','$compile','$rootScope',function($document,$compile,$rootScope){

                var body = $document.find('body'), $scope, $element, $popupContainer;

                function initPopup(includingHtml) {
                    ui.debugLog('Init popup');
                    $popupContainer = angular.element('<div class="b-popup"><div class="b-popup__bg"></div><div class="b-popup__content">'+includingHtml+'</div></div>');
                    $compile($popupContainer)($scope);
                    body.append($popupContainer);
                }

                function attachUserLoginForm() {
                    ui.debugLog('Init login form');
                    initPopup('<div onload="formLoaded()" ng-include="'+"'/templates/forms/login.html'"+'"></div>');
                }

                function bindContainerClose() {
                    $popupContainer.find('.b-popup__bg').click(function(){
                        cleanUp();
                    });
                }

                function cleanUp() {
                    $popupContainer.remove();
                }

                function formLoaded() {
                    ui.debugLog('Login form loaded');
                }

                function link(scope,element, attrs) {
                    $scope = $rootScope.$new();
                    $scope.formLoaded = formLoaded;
                    $element = element;

                    switch(attrs.cdPopup) {
                        case 'login':
                            $scope.close = cleanUp;
                            $($element).on('click',function(e){
                                $scope.$apply(function(){
                                    attachUserLoginForm();
                                    bindContainerClose();
                                });
                                e.preventDefault();
                            });
                        break;
                    }
                }
                return {
                    link: link
                };
            }])
    }
);