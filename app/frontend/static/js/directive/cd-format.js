define([
    './module.js',
    'jquery'
],
    function (directives,$) {

        'use strict';

        directives
            .directive('cdFormat',function(){

                function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
                    //
                    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
                    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                    // +	 bugfix by: Michael White (http://crestidg.com)

                    var i, j, kw, kd, km;

                    // input sanitation & defaults
                    if( isNaN(decimals = Math.abs(decimals)) ){
                        decimals = 2;
                    }
                    if( dec_point == undefined ){
                        dec_point = ",";
                    }
                    if( thousands_sep == undefined ){
                        thousands_sep = ".";
                    }

                    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

                    if( (j = i.length) > 3 ){
                        j = j % 3;
                    } else{
                        j = 0;
                    }

                    km = (j ? i.substr(0, j) + thousands_sep : "");
                    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
                    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
                    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


                    return km + kw + kd;
                }


                function formatRusPrice(value) {
                    value = value.replace(/\D/g,'');
                    return number_format(value,0,'.',' ');
                }

                function link(scope,element, attrs) {

                    switch(attrs.cdFormat) {
                        case 'price':
                            if(element.has('input'))
                                element.on('change',function(e){
                                    element.val(formatRusPrice(element.val()));
                                });
                            break;
                    }
                }
                return {
                    link: link
                };
            })
    }
);