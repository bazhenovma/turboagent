define(['./module.js','ui'], function (services,ui) {
    'use strict';
    services
        .factory('geoCrud', ["$rootScope","$controller","$http","createPopupMessage",
            function ($rootScope, $controller, $http, createPopupMessage) {

                function parseFieldsErrors(form,fields) {
                    for(var index in fields) {
                        form[index].$setValidity(fields[index].type, fields[index].val);
                    }
                }

                return function($scope) {
                    $scope.messages = {};
                    $scope.crudUrl = {};

                    $scope.add = function() {
                        if($scope.newForm.$invalid) {
                            return;
                        }

                        $http.post($scope.crudUrl.add, $scope.new)
                            .success(function(res) {
                                createPopupMessage('success',{
                                    message: $scope.messages.addSuccess
                                });
                                $scope.getAll();
                            })
                            .error(function(data,status){
                                if(data.hasOwnProperty('errors')) {
                                    parseFieldsErrors($scope.newForm, data.errors);
                                }
                                $rootScope.$emit('HTTP_RESPONSE',data,status);
                            });
                    };

                    $scope.update = function() {
                        $http.post($scope.crudUrl.update, $scope.updateData())
                            .success(function() {
                                createPopupMessage('success',{
                                    message: $scope.messages.updateSuccess
                                });
                                $scope.getAll();
                                delete($scope.edit);
                            })
                            .error(function(data,status){
                                if(data.hasOwnProperty('errors')) {
                                    parseFieldsErrors($scope.updateForm, data.errors);
                                }
                                $rootScope.$emit('HTTP_RESPONSE',data,status);
                            });
                    };
                    $scope.getAll = function(url,callback) {
                        var getUrl = url ? url : typeof $scope.crudUrl.getAll == 'function' ? $scope.crudUrl.getAll():$scope.crudUrl.getAll ;

                        if(getUrl)
                            $http.get(getUrl)
                                .success(function(data){
                                    if(callback)
                                        callback(data);
                                    else
                                        $scope.base = data;
                                    ui.updateControls();
                                })
                                .error(function(data,status){
                                    $rootScope.$emit('HTTP_RESPONSE',data,status);
                                });
                    };
                    $scope.doEdit = function(item) {
                        $scope.edit = item;
                        ui.updateControls();
                    };
                    $scope.doRemove = function(item) {
                        if(confirm('Вы действительно хотите удалить?')) {
                            $http.post($scope.crudUrl.remove, item)
                                .success(function() {
                                    createPopupMessage('success',{
                                        message: $scope.messages.removeSuccess
                                    });
                                    $scope.base.splice($scope.base.indexOf(item),1);
                                })
                                .error(function(data,status){
                                    $rootScope.$emit('HTTP_RESPONSE',data,status);
                                });
                        }
                    }
                }
            }]);
});