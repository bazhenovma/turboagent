define(['./module.js'], function (services) {
    'use strict';
    services
        .factory('createPopupMessage', ["$document", "$compile", "$rootScope", "$controller", "$timeout",
            function ($document, $compile, $rootScope, $controller, $timeout) {
                var defaults = {
                    message: 'Default Message',
                    timeout: 9000
                };

                var modalContainer = $document.find('.b-popup-messages');

                return function(type, options) {
                    options = angular.extend({}, defaults, options); //options defined in constructor

                    switch (type) {
                        case 'success':
                            options.templateUrl = '/templates/service/popup_message/success.html';
                        break;
                        case 'warning':
                            options.templateUrl = '/templates/service/popup_message/warning.html';
                        break;
                        case 'error':
                            options.templateUrl = '/templates/service/popup_message/error.html';
                        break;
                        default:
                            console.error('Type for popup message undefined');
                            return;
                    }

                    var modalBody = '<div ng-include="\'' + options.templateUrl + '\'"></div>';

                    var modalEl = angular.element(
                        '<div class="b-popup-message b-popup-message_state_hidden">' +
                            '<button type="button" class="b-close" ng-click="$modalClose()">&times;</button>' +
                            modalBody +
                        '</div>'
                    );

                    var closePopupMessage = function () {
                        modalEl.addClass('b-popup-message_state_closed');
                        $timeout(function () {
                            modalEl.remove();
                        }, 2000);
                    };

                    var scope = options.scope || $rootScope.$new();

                    scope.message = options.message;
                    scope.$modalClose = closePopupMessage;

                    $compile(modalEl)(scope);
                    modalContainer.append(modalEl);

                    $timeout(function () {
                        modalEl.removeClass('b-popup-message_state_hidden');
                    }, 1500);

                    $timeout(function () {
                        closePopupMessage();
                    }, options.timeout);
                };
            }]);
});