define(['./module.js'], function (services) {
    'use strict';
    services
        .factory('authorization', ["$rootScope","$window",
            function ($rootScope, $window) {
                var doAuth = function(data) {
                    if(data.token && data.id && data.role) {
                        $rootScope.userData = data;
                        $rootScope.setAuthHeaders(data);

                        if($window.localStorage)
                            $window.localStorage.u_t = data.token;
                    }
                }

                var isAuth = function() {
                    if($rootScope.userData)
                        return true;

                    return false;
                }

                var logout = function() {
                    if(isAuth()) {
                        delete $rootScope.userData;
                        delete $window.localStorage.u_t;
                    }
                }

                var checkAccess = function(state) {
                    if(state.access.users) {
                        if(state.access.users == '*')
                            return true;

                        if(state.access.users == '~') {
                            return $rootScope.userData ? false : true;
                        }

                        if(state.access.users == '@') {
                            return $rootScope.userData ? true : false;
                        }
                    }
                    if(state.access.roles) {
                        if($rootScope.userData)
                            return state.access.roles.indexOf($rootScope.userData.role) != -1;
                        else
                            return false;
                    }
                }

                return {
                    checkAccess: checkAccess,
                    doAuth: doAuth,
                    isAuth: isAuth,
                    logout: logout
                }
            }]);
});