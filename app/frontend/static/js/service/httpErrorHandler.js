define(['./module.js','ui'], function (services,ui) {
    'use strict';
    services
        .factory('errorHandler.factory', ["$rootScope","createPopupMessage",
            function ($rootScope,$popupMessage) {

                function handleResponse(event,data,status) {
                    if([400,404,500].indexOf(status) !== -1) {
                        if(data.message) {
                            $popupMessage('error',{
                                message: data.message
                            });
                        }
                        if(status == 0) {
                            $popupMessage('error',{
                                message: 'Сервер не отвечает, попробуйте позже.'
                            });
                        } else {
                            $popupMessage('error',{
                                message: 'Ошибка '+ status + (data.hasOwnProperty('text') ? ' : ' + data.text : '')
                            });
                        }
                        ui.debugLog('EVENT',event,data,status);
                    }
                }

                return {
                    init: function() {
                        $rootScope.$on('HTTP_RESPONSE',handleResponse);
                    }
                }

            }]);
});