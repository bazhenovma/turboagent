require.config({
    baseUrl: '/js/dashboard/',
    // alias libraries paths
    paths: {
        'domReady': '/js/libs/domReady',
        'angular': '/js/libs/angular.min',
        'underscore': '/js/libs/underscore-min',
        'angular.animate': '/js/libs/angular-animate.min',
        'angular.loadingBar': '/js/libs/loading-bar.min',
        'angular.ui.router': '/js/libs/angular-ui-router.min',
        'jquery': '/js/libs/jquery.min',
        'chosen': '/js/libs/chosen.jquery.min',
        'ui':'/js/ui/index'
    },

    // angular does not support AMD out of the box, put it in a shim
    shim: {
        'jquery': {
            exports: 'jQuery'
        },
        'chosen': {
            deps:  ['jquery']
        },
        'ui': {
            deps:  ['jquery','chosen']
        },
        'angular': {
            deps: ['jquery'],
            exports: 'angular'
        },
        'angular.ui.router': ['angular'],
        'angular.animate': ['angular'],
        'angular.loadingBar': ['angular']
    },
    // kick start application
    deps: ['/js/dashboard/bootstrap.js'],
    out: "dashboard.built.js"
});