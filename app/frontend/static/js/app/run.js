define([
    './app',
    'ui'
], function (app,ui) {
    'use strict';

    return app.run(['cfpLoadingBar','$rootScope','$state','$stateParams','$templateCache','authorization','$http','$window','errorHandler.factory', function (cfpLoadingBar,$rootScope,$state,$stateParams,$templateCache,secure,$http,$window,$errorHandler) {

        $rootScope.title = 'Турбо Агент';
        $rootScope.server = { search: 'http://localhost:3000', dashboard: 'http://localhost:3001'};
        $rootScope.global_loading = true;
        $errorHandler.init();
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.getCurrentUser = function(callback) {
            if(!$rootScope.currentUser) {
                $http.get($rootScope.server.dashboard+'/api/user/getCurrent/')
                    .success(function(response){
                        $rootScope.currentUser = response;
                        callback(response);
                    })
                    .error(function(data,status){
                        callback(null);
                        $rootScope.handleRequestError(data,status);
                    });
            } else {
                callback($rootScope.currentUser);
            }
        }

        $rootScope.checkAccess = function(auth_required) {
            var auth = secure.isAuth();

            return (auth_required && auth) || (!auth_required && !auth);
        }

        $rootScope.completeLoadingBar = function () {
            cfpLoadingBar.complete();
        };

        $rootScope.startLoadingBar = function() {
            cfpLoadingBar.start();
        };
        $rootScope.$on("$stateChangeStart", function (event, next, current) {
            if(!secure.checkAccess(next)) {
                $rootScope.$emit('ACCESS_DENIED');
                next = current;
            }
            $rootScope.startLoadingBar();
            $templateCache.removeAll();
        });
        $rootScope.$on("$stateChangeSuccess", function (event, next, current) {
            $rootScope.completeLoadingBar();
        });
        $rootScope.$on("$stateChangeError", function (event, next, current) {
            $rootScope.completeLoadingBar();
        });
        $rootScope.$on('$viewContentLoaded',function(event) {
            ui.controls.init();
            ui.updateControls();
        });
        $rootScope.setAuthHeaders = function(data) {
            $http.defaults.headers.common["x-user-id"] = data.id;
            $http.defaults.headers.common["x-user-token"] = data.token;
        }

        if($window.localStorage.u_t) {
            $http.post($rootScope.server.dashboard + '/api/load/token/',{token: $window.localStorage.u_t})
                .success(function(res){
                   secure.doAuth(res);
                })
                .error(function(){
                    delete $window.localStorage.u_t;
                });
        }

    }]);
});