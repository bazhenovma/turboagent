define([
    'angular',
    './controller/index',
    '/js/service/index.js',
    '/js/directive/index.js',
    'angular.ui.router',
    'angular.animate',
    'angular.loadingBar'
], function (ng) {
    'use strict';

    return ng.module('app', [
        'app.controllers',
        'app.services',
        'app.directives',
        'ngAnimate',
        'chieffancypants.loadingBar',
        'ui.router'
    ]);
});