define([
    './app'
], function (app) {
    'use strict';

    return app.config(['$stateProvider','$urlRouterProvider','$httpProvider','$locationProvider', function ($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider) {

        $locationProvider.html5Mode(true);

        $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
        $httpProvider.defaults.transformRequest = function(data) {
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        };
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/templates/section/home.html',
                controller: 'home',
                access: {
                    users: '*'
                }
            })
            .state('login', {
                url: '/login/',
                access: {
                    users: '~'
                }
            })
            .state('logout', {
                url: '/logout/',
                templateUrl: '/templates/section/logout.html',
                controller: 'logout',
                access: {
                    users: '@'
                }
            })
            .state('error.404',{
                url:'/error/404/',
                templateUrl: '/templates/error/404.html',
                controller: ['$location','$locationProvider',function($rootScope) {
                    $rootScope.page_name = '/ Страница не найдена / Ошибка 404';
                }]
            });

        $urlRouterProvider
            .when('','home')
            .otherwise('/error/404/');
    }]);
});