define([
    'require',
    'angular',
    'ui',
    'jquery',
    'app',
    'run',
    'config'
], function (require, ng, ui, $) {
    'use strict';

    $(window).ready(function(){
        ui.app.init();
        ng.bootstrap(document, ['app']);
    })
});