define([
    './module'
],
    function (controllers) {
        'use strict';

        controllers
            .controller('login',['$scope','$rootScope','$document','createPopupMessage','$http','authorization', function ($scope,$rootScope,$document,createPopupMessage,$http,secure) {
                $rootScope.page_name = '/ Авторизация';
                $scope.account = { email: 'imike.rus@gmail.com', password: 123456 };

                $scope.doLogin = function() {
                    if($scope.loading == true)
                        return;

                    $scope.loading = true;

                    $http.post($rootScope.server.dashboard+'/api/login/', $scope.account)
                        .success(function(res){
                            createPopupMessage('success',{
                                message: 'Авторизация выполнена'
                            });
                            secure.doAuth(res);
                            $scope.loading = false;
                            if($scope.close)
                                $scope.close();
                            if(window.localStorage.returnUrl) {
                                var returnUrl = window.localStorage.returnUrl;
                                delete window.localStorage.returnUrl;
                                window.location.href = returnUrl
                            }
                        })
                        .error(function(data, status, headers, config) {
                            $scope.loading = false;
                            if(data.message) {
                                createPopupMessage('error',{
                                    message: data.message
                                });
                            }
                            if(status == 0) {
                                createPopupMessage('error',{
                                    message: 'Сервер не отвечает, попробуйте позже.'
                                });
                            } else {
                                createPopupMessage('error',{
                                    message: 'Ошибка '+ status + (data ? ' : ' + data : '')
                                });
                            }
                        });
                }

                $scope.togglePassword = function() {
                    var el = $document.find('.b-form-field_type_password');
                    if(el.attr('type') == 'password')
                        el.attr('type','text');
                    else
                        el.attr('type','password');
                }
            }])
            .controller('logout',['$scope','$rootScope','authorization', function ($scope,$rootScope,secure) {
                $rootScope.page_name = '/ Выход';
                secure.logout();
            }]);
    }
);