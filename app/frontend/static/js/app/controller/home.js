define([
    './module',
    'ui'
],
    function (controllers,ui) {
        'use strict';

        controllers
            .controller('home',['$scope','$rootScope', function ($scope,$rootScope) {
                $rootScope.page_name = '/ Поиск недвижимости';

                $scope.formLoaded = function() {
                    $rootScope.global_loading = false;
                }
            }])
            .controller('saveFilter',['$scope','$rootScope', function ($scope,$rootScope) {
                $scope.$parent.$watch('showSaveFilter',function(newValue,oldValue){
                    if(newValue == true) {
                        if(!$scope.saveFilter) {
                            $scope.saveFilter = {};
                            $rootScope.getCurrentUser(function(user){
                                if(user) {
                                    $scope.saveFilter.email = user.email;
                                }
                                ui.controls.removeAnimateToRow('saveFilter.email');
                            });
                            ui.controls.addAnimateToRow('saveFilter.email','Выполняется загрузка персональных данных');
                        } else {
                            if(!$scope.saveFilter.email) {
                                $rootScope.getCurrentUser(function(user){
                                    if(user) {
                                        $scope.saveFilter.email = user.email;
                                    }
                                    ui.controls.removeAnimateToRow('saveFilter.email');
                                });
                                ui.controls.addAnimateToRow('saveFilter.email','Выполняется загрузка персональных данных');
                            }
                        }
                    }
                });
            }])
            .controller('SearchCtrl', ['$scope','$http',function ($scope, $http) {
                switch($scope.search_type) {
                    case 1:
                        $scope.searchPlaceholder = 'Петров Иван Викторович';
                        break;
                    case 2:
                        $scope.searchPlaceholder = 'МОУ СОШ №2';
                        break;
                    case 3:
                        $scope.searchPlaceholder = 'Красноярск';
                        break;
                    default:
                        $scope.searchPlaceholder = 'Петров Иван Викторович';
                }

                $scope.submit=function(){
                    console.log(angular.toJson($scope.search));
                }
            }]);
    }
);