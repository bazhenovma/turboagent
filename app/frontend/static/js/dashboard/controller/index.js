define([
    './dashboard',
    './settings',
    './settings.geo',
    './settings.request.queries',
    './adverts',
    './statistic',
    './users'
], function () {});