define([
    './module',
    'jquery'
],
    function (controllers,$) {
        'use strict';

        controllers
            .controller('statistic',['$scope','$rootScope', function ($scope,$rootScope) {
                $rootScope.page_name = '/ ЦПУ / Статистика';
                $rootScope.breadcrumbs.section = { name: 'Статистика', state: 'statistic'};
            }])

    }
);