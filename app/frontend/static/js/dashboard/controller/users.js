define([
    './module',
    'jquery',
    'ui'
],
    function (controllers,$,ui) {
        'use strict';

        controllers
            .controller('users',['$scope','$rootScope', function ($scope,$rootScope) {
                $rootScope.page_name = '/ ЦПУ / Пользователи';
                $rootScope.breadcrumbs.section = { name: 'Пользователи', state: 'users'};
            }])
            .controller('users.add',['$scope','$rootScope','$http','createPopupMessage', function ($scope,$rootScope,$http,createPopupMessage) {
                $rootScope.page_name = '/ ЦПУ / Пользователи / Добавить';
                $rootScope.breadcrumbs.section = { name: 'Пользователи', state: 'users'};

                $http.get($rootScope.server.dashboard+'/security/dashboard/get/users/roles/')
                    .success(function(res){
                        $scope.roles = res;
                        ui.updateControls();
                    })
                    .error($rootScope.handleRequestError);

                $http.get($rootScope.server.dashboard+'/security/dashboard/get/users/actions/')
                    .success(function(res){
                        $scope.actions = res;
                        ui.updateControls();
                    })
                    .error($rootScope.handleRequestError);

                $http.get($rootScope.server.dashboard+'/security/dashboard/get/users/statuses/')
                    .success(function(res){
                        $scope.statuses = res;
                        ui.updateControls();
                    })
                    .error($rootScope.handleRequestError);

                $scope.add = function() {
                    $http.post($rootScope.server.dashboard+'/api/dashboard/add/user/', $.param($scope.user))
                        .success(function(res){
                            createPopupMessage('success',{
                                message: 'Пользователь добавлен'
                            })
                        })
                        .error($rootScope.handleRequestError);
                }
            }])

    }
);