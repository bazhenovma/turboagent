define([
    './module',
    'jquery'
],
    function (controllers,$) {
        'use strict';

        controllers
            .controller('dashboard',['$scope','$rootScope','$http','createPopupMessage', function ($scope,$rootScope,$http,createPopupMessage) {
                $rootScope.page_name = '/ ЦПУ';
                $rootScope.breadcrumbs = {};

                $http.get($rootScope.server.dashboard+'/api/dashboard/get/nav/primary/')
                    .success(function(data){
                        $rootScope.hidePage = false;
                        $rootScope.primary_nav = data;
                    })
                    .error($rootScope.handleRequestError);
            }]);
    }
);