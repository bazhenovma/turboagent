define([
    './module',
    'jquery'
],
    function (controllers,$) {
        'use strict';

        controllers
            .controller('settings', ['$scope','$rootScope','$http',function ($scope,$rootScope, $http) {
                $rootScope.page_name = '/ ЦПУ / Настройки портала';
                $rootScope.breadcrumbs.section = { name: 'Настройки портала', state: 'settings'};
            }])
            .controller('settings.requests.classifier', ['$scope','$rootScope','$http','createPopupMessage',function ($scope,$rootScope, $http,createPopupMessage) {
                $rootScope.page_name = '/ ЦПУ / Настройки поиска / Классификатор запросов';
                $rootScope.breadcrumbs.current = { name: 'Классификатор запросов'};

                $http.get($rootScope.server.search+'/api/search/get/classification/base/')
                    .success(function(data) {
                        if(data.length == 0)
                            $scope.base = [];
                        else
                            $scope.base = data;
                    })
                    .error($rootScope.handleRequestError);

                $http.get($rootScope.server.search+'/api/search/getUserQueries/')
                    .success(function(data) {
                        if(data.length == 0)
                            $scope.userQueries = [];
                        else
                            $scope.userQueries = data;
                    })
                    .error($rootScope.handleRequestError);

                $scope.setQueryText = function(text) {
                    $scope.text = text;
                }

                $scope.classifyQueryText = function() {
                    if(typeof $scope.text == 'undefined') {
                        createPopupMessage('warning',{
                            message: 'Заполните форму формализации запроса'
                        });
                        return;
                    }
                    $http.get($rootScope.server.search+'/api/search/formalize/'+$scope.text+'/')
                        .success(function(data) {
                            if(data == 'null')
                                createPopupMessage('warning',{
                                   message: 'Система не обучена, либо не смогла формализовать запрос'
                                });
                            else {
                                $scope.query = data;
                                setTimeout(function(){
                                    $(".b-form-field_type_select").trigger("chosen:updated");
                                    $('.b-button input').change();
                                },300);
                            }
                        })
                        .error($rootScope.handleRequestError);
                }

                $scope.flushQuery = function() {
                    $scope.query = null;

                    setTimeout(function(){
                        $(".b-form-field_type_select").trigger("chosen:updated");
                        $('.b-button input').change();
                    },300);
                }

                $scope.saveFormalizeQuery = function() {
                    if(typeof $scope.query == 'undefined' || $scope.query == 'null') {
                        createPopupMessage('warning',{
                            message: 'Заполните форму формирования запроса'
                        });
                        return;
                    }
                    if(typeof $scope.text == 'undefined') {
                        createPopupMessage('warning',{
                            message: 'Заполните форму формализации запроса'
                        });
                        return;
                    }
                    $http.post($rootScope.server.search+'/api/search/add/train/query/'+$scope.text+'/', $.param($scope.query))
                        .success(function() {
                            $scope.base.push({input: $scope.text, output: $scope.query});
                        })
                        .error($rootScope.handleRequestError);
                }

            }]);

    }
);