define([
    './module',
    'jquery'
],
    function (controllers,$) {
        'use strict';

        controllers
            .controller('settings.requests.queries', ['$scope','$rootScope','$http',function ($scope,$rootScope) {
                $rootScope.page_name = '/ ЦПУ / Настройки поиска / Запросы';
                $rootScope.breadcrumbs.current = { name: 'Запросы'};
            }])
            .controller('settings.requests.queries.sort', ['$scope','$rootScope','createPopupMessage','$http',function ($scope,$rootScope,createPopupMessage, $http) {
                var currentDate = new Date(),
                    months = ['Январь','Февраль','Март','Май','Июнь','Июль','Сентябрь','Октябрь','Ноябрь','Декабрь','Январь','Январь'],
                    arrForTextSorting;

                $scope.open = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.opened = true;
                };

                $scope.sortQueries = function() {
                    var date = currentDate;

                    if(!$scope.queries_base)
                        return;
                    if($scope.sort)
                        date = typeof $scope.sort.date != 'undefined' ? $scope.sort.date : currentDate;
                    else
                        $scope.sort = {};

                    $scope.sort.month = months[date.getMonth()];
                    $scope.sort.year = date.getFullYear();
                    $scope.sort.day = date.getDate();

                    var day = new Date(date.getFullYear(),date.getMonth(),date.getDate()).getTime();
                    var month = new Date(date.getFullYear(),date.getMonth()).getTime();

                    $scope.sort.base = [];
                    for(var i = 0, queriesLength = $scope.queries_base.length; i < queriesLength; i++) {
                        var query = {
                            id: $scope.queries_base[i]._id,
                            text: $scope.queries_base[i].text,
                            quantity: $scope.queries_base[i].queries,
                            month: { quantity: $scope.queries_base[i].month[month] ? $scope.queries_base[i].month[month].quantity : 0},
                            day: { quantity: $scope.queries_base[i].day[day] ? $scope.queries_base[i].day[day].quantity : 0}
                        };

                        if($scope.sort.date) {
                            if($scope.queries_base[i].month[month] && $scope.queries_base[i].day[day])
                                $scope.sort.base.push(query);

                        } else {
                            $scope.sort.base.push(query);
                        }
                    }
                    arrForTextSorting = $scope.sort.base;
                }

                $scope.sortByText = function() {
                    if($scope.sort.text.length != 0) {
                        var buf = arrForTextSorting;
                        $scope.sort.base = [];

                        for(var i = 0, queriesLength = buf.length; i < queriesLength; i++) {
                            if(buf[i].text.toLowerCase().indexOf($scope.sort.text.toLowerCase()) != -1) {
                                $scope.sort.base.push(buf[i]);
                            }
                        }
                    } else {
                        $scope.sortQueries();
                    }
                }

                $http.get($rootScope.searchServer+'/api/search/getUserQueries/')
                    .success(function(data){
                        $scope.queries_base = data;
                        $scope.sortQueries();
                    })
                    .error(function(data, status, headers, config){
                        console.log(data,status,headers,config);
                        if(data.message) {
                            createPopupMessage('error',{
                                message: data.message
                            });
                        }
                        if(status) {
                            createPopupMessage('error',{
                                message: 'Ошибка '+status
                            });
                        }
                    });
            }])
            .controller('AddSearchQuery', ['$scope','$http','$state','createPopupMessage','$rootScope',function ($scope, $http,$state,createPopupMessage,$rootScope) {
                $scope.submit=function(){
                    $http.post($rootScope.searchServer+'/api/search/add/query/', $.param($scope.formdata))
                        .success(function() {
                            $state.go($state.$current, null, { reload: true });
                        })
                        .error($rootScope.handleRequestError);
                }
            }]);

    }
);