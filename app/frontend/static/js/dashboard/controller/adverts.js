define([
    './module',
    'jquery',
    'ui'
],
    function (controllers,$,ui) {
        'use strict';

        controllers
            .controller('adverts',['$scope','$rootScope', function ($scope,$rootScope) {
                $rootScope.page_name = '/ ЦПУ / Объявления';
                $rootScope.breadcrumbs.section = { name: 'Объявления', state: 'adverts'};
            }])
            .controller('adverts.new',['$scope','$rootScope','$document','$compile','$http','createPopupMessage', function ($scope,$rootScope,$document,$compile,$http,createPopupMessage) {
                $rootScope.page_name = '/ ЦПУ / Объявления / Добавить новое';
                $rootScope.breadcrumbs.current = { name: 'Добавить новое'};

                var object_container = $document.find('.b-object-params-container');

                $scope.object_types = [
                    {
                        ID: 1,
                        name: 'Квартира'
                    },{
                        ID: 2,
                        name: 'Гостинка'
                    },{
                        ID: 3,
                        name: 'Участок земли'
                    },{
                        ID: 4,
                        name: 'Склад'
                    },{
                        ID: 5,
                        name: 'Офис'
                    },{
                        ID: 6,
                        name: 'Нежилое помещение'
                    }
                ];

                $scope.$watch('query.advert.object_type',function(newValue, oldValue){
                    if(!newValue)
                        return;

                    object_container.innerHTML = '';
                    var url;
                    //TODO заменить тип объектов на список из базы
                    switch (newValue) {
                        case 1:
                            url = '/templates/page/dashboard/adverts/apartment/form.html';
                        break;
                    }
                    var layoutForm = angular.element('<div ng-include="\'' + url + '\'"></div>'),
                        element = $compile(layoutForm)($scope);

                    object_container.append(element);
                    ui.updateControls();
                });

                $scope.$watch('query.advert.type',function(newValue, oldValue){
                    //TODO заменить на получение с сервера списка величин по валютам
                    if(!newValue)
                        return;
                    if(!$scope.query.object)
                        $scope.query.object = {};

                    if(!$scope.query.object.price)
                        $scope.query.object.price = {};

                    switch(newValue) {
                        case '1':
                            $scope.query.object.price.measure = 'руб/мес.';
                        break;
                        case '2':
                            $scope.query.object.price.measure = 'руб';
                        break;
                    }
                });

                $scope.add = function() {
                    $http.post($rootScope.server.dashboard+'/api/dashboard/advert/add/', $.param($scope.query))
                        .success(function(res) {
                            createPopupMessage('success',{
                                message: 'Объявление опубликованно'
                            });
                        })
                        .error($rootScope.handleRequestError);
                }

                ui.updateControls();
            }])

    }
);