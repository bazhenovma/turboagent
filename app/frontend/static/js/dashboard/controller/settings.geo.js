define([
    './module',
    'jquery',
    'underscore',
    'ui'
],
    function (controllers,$,_,ui) {
        'use strict';

        controllers
            .controller('settings.geo_countries', ['$scope','$rootScope','geoCrud',function ($scope,$rootScope,geoCrud) {
                $rootScope.page_name = '/ ЦПУ / Настройки гео данных / Страны';
                $rootScope.breadcrumbs.current = { name: 'Страны'};

                geoCrud($scope);

                $scope.messages = {
                    addSuccess: 'Страна добавлена в базу',
                    updateSuccess: 'Страна отредактированна',
                    removeSuccess: 'Страна удалена'
                };

                $scope.crudUrl = {
                    add: $rootScope.server.dashboard+'/api/dashboard/add/country/',
                    update: $rootScope.server.dashboard+'/api/dashboard/update/country/',
                    getAll: $rootScope.server.dashboard+'/api/dashboard/get/all/countries/',
                    remove: $rootScope.server.dashboard+'/api/dashboard/remove/country/'
                };

                $scope.updateData = function() {
                    return {
                        _id: $scope.edit._id,
                        data: _.pick($scope.edit,'name','currency')
                    }
                }

                //Устанавливаем валидация для поля name
                $scope.$on('$viewContentLoaded', function() {
                    $scope.newForm.name.$setValidity('unique', true);
                    $scope.updateForm.name.$setValidity('unique', true);

                    $scope.$watch('new.name',function(newValue,oldValue){
                        $scope.newForm.name.$setValidity('unique', true);
                    });
                    $scope.$watch('edit.name',function(newValue,oldValue){
                        $scope.updateForm.name.$setValidity('unique', true);
                    });
                });

                $scope.getAll();
            }])
            .controller('settings.geo_areas', ['$scope','$rootScope','geoCrud',function ($scope,$rootScope,geoCrud) {
                $rootScope.page_name = '/ ЦПУ / Настройки гео данных / Регионы';
                $rootScope.breadcrumbs.current = { name: 'Регионы'};

                geoCrud($scope);

                $scope.messages = {
                    addSuccess: 'Регион добавлен в базу',
                    updateSuccess: 'Регион отредактирован',
                    removeSuccess: 'Регион удален'
                };

                $scope.getAllUrl = function() {
                    return $scope.country ? $rootScope.server.dashboard+'/api/dashboard/get/all/areas/'+$scope.country._id+'/':null;
                }

                $scope.crudUrl = {
                    add: $rootScope.server.dashboard+'/api/dashboard/add/area/',
                    update: $rootScope.server.dashboard+'/api/dashboard/update/area/',
                    getAll: $scope.getAllUrl,
                    remove: $rootScope.server.dashboard+'/api/dashboard/remove/area/'
                };

                $scope.updateData = function() {
                    return {
                        _id: $scope.edit._id,
                        data: _.pick($scope.edit,'name','_country')
                    }
                }

                $scope.loadDependencies = function() {
                    $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/countries/',function(data){
                        $scope.countries = data;
                    });
                }

                $scope.$watch('country',function(newValue, oldValue){
                    if(newValue) {
                        $scope.getAll();
                        if(!$scope.new)
                            $scope.new = {};

                        $scope.new._country = newValue;
                    }
                });

                $scope.loadDependencies();
                $scope.getAll();
            }])
            .controller('settings.geo_towns', ['$scope','$rootScope','geoCrud',function ($scope,$rootScope,geoCrud) {
                $rootScope.page_name = '/ ЦПУ / Настройки гео данных / Города';
                $rootScope.breadcrumbs.current = { name: 'Города'};

                geoCrud($scope);

                $scope.messages = {
                    addSuccess: 'Город добавлен в базу',
                    updateSuccess: 'Город отредактирован',
                    removeSuccess: 'Город удален'
                };

                $scope.getAllUrl = function() {
                    return $scope.area ? $rootScope.server.dashboard+'/api/dashboard/get/all/towns/'+$scope.area._id+'/':null;
                }

                $scope.crudUrl = {
                    add: $rootScope.server.dashboard+'/api/dashboard/add/town/',
                    update: $rootScope.server.dashboard+'/api/dashboard/update/town/',
                    getAll: $scope.getAllUrl,
                    remove: $rootScope.server.dashboard+'/api/dashboard/remove/town/'
                };

                $scope.updateData = function() {
                    return {
                        _id: $scope.edit._id,
                        data: _.pick($scope.edit,'name','_area')
                    }
                }

                $scope.$watch('country',function(newValue, oldValue){
                    if(newValue) {
                        $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/areas/'+newValue._id+'/',function(data){
                            $scope.areas = data;
                        });
                        if(!$scope.new)
                            $scope.new = {};

                        $scope.new._country = newValue;
                    }
                });

                $scope.$watch('area',function(newValue, oldValue){
                    if(newValue) {
                        $scope.getAll();
                        if(!$scope.new)
                            $scope.new = {};

                        $scope.new._area = newValue;
                    }
                });

                $scope.getAll();
            }])
            .controller('settings.geo_districts', ['$scope','$rootScope','geoCrud',function ($scope,$rootScope,geoCrud) {
                $rootScope.page_name = '/ ЦПУ / Настройки гео данных / Районы';
                $rootScope.breadcrumbs.current = { name: 'Районы'};
                $scope.town_base = [$rootScope.geo_selector.get('town')];
                $scope.new._town = $rootScope.geo_selector.get('town');

                geoCrud($scope);

                $scope.messages = {
                    addSuccess: 'Район добавлен в базу',
                    updateSuccess: 'Район отредактирован',
                    removeSuccess: 'Район удален'
                };

                $scope.getAllUrl = function() {
                    return $scope.town ? $rootScope.server.dashboard+'/api/dashboard/get/all/districts/'+$scope.town._id+'/':null;
                }

                $scope.crudUrl = {
                    add: $rootScope.server.dashboard+'/api/dashboard/add/district/',
                    update: $rootScope.server.dashboard+'/api/dashboard/update/district/',
                    getAll: $scope.getAllUrl,
                    remove: $rootScope.server.dashboard+'/api/dashboard/remove/district/'
                };

                $scope.updateData = function() {
                    return {
                        _id: $scope.edit._id,
                        data: _.pick($scope.edit,'name','_town')
                    }
                }

                $scope.$watch('area',function(newValue, oldValue){
                    if(newValue) {
                        $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/towns/'+newValue._id+'/',function(data){
                            $scope.towns = data;
                            
                        });
                    }
                });

                $scope.$watch('town',function(newValue, oldValue){
                    if(newValue) {
                        if(!_.isEmpty(newValue)) {
                            $scope.getAll();
                            if(!$scope.new)
                                $scope.new = {};

                            $scope.new._town = newValue;
                            
                        }
                    }
                });

                $scope.getAll();
            }])
            .controller('settings.geo_streets', ['$scope','$rootScope','geoCrud',function ($scope,$rootScope,geoCrud) {
                $rootScope.page_name = '/ ЦПУ / Настройки гео данных / Улицы';
                $rootScope.breadcrumbs.current = { name: 'Улицы'};

                geoCrud($scope);

                $scope.messages = {
                    addSuccess: 'Улица добавлена в базу',
                    updateSuccess: 'Улица отредактирована',
                    removeSuccess: 'Улица удалена'
                };

                $scope.getAllUrl = function() {
                    return $scope.town ? $rootScope.server.dashboard+'/api/dashboard/get/all/streets/'+$scope.town._id+'/':null;
                }

                $scope.crudUrl = {
                    add: $rootScope.server.dashboard+'/api/dashboard/add/street/',
                    update: $rootScope.server.dashboard+'/api/dashboard/update/street/',
                    getAll: $scope.getAllUrl,
                    remove: $rootScope.server.dashboard+'/api/dashboard/remove/street/'
                };

                $scope.updateData = function() {
                    return {
                        _id: $scope.edit._id,
                        data: _.pick($scope.edit,'name','_town','_district')
                    }
                }

                $scope.$watch('town',function(newValue, oldValue){
                    if(newValue) {
                        if(!_.isEmpty(newValue)) {
                            $scope.getAll();
                            if(!$scope.new)
                                $scope.new = {};

                            $scope.new._town = newValue;

                            $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/districts/'+newValue._id+'/',function(data){
                                $scope.districts = data;
                            });
                        }
                    }
                });

                $scope.$watch('area',function(newValue, oldValue){
                    if(newValue) {
                        $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/towns/'+newValue._id+'/',function(data){
                            $scope.towns = data;

                        });
                    }
                });

                $scope.getAll();
            }])
            .controller('settings.geo_addresses', ['$scope','$rootScope','geoCrud',function ($scope,$rootScope, geoCrud) {
                $rootScope.page_name = '/ ЦПУ / Настройки гео данных / Адреса';
                $rootScope.breadcrumbs.current = { name: 'Адреса'};

                geoCrud($scope);

                $scope.messages = {
                    addSuccess: 'Адрес добавлен в базу',
                    updateSuccess: 'Адрес отредактирован',
                    removeSuccess: 'Адрес удален'
                };

                $scope.getAllUrl = function() {
                    return $scope.street ? $rootScope.server.dashboard+'/api/dashboard/get/all/addresses/'+$scope.street._id+'/':null;
                }

                $scope.crudUrl = {
                    add: $rootScope.server.dashboard+'/api/dashboard/add/address/',
                    update: $rootScope.server.dashboard+'/api/dashboard/update/address/',
                    getAll: $scope.getAllUrl,
                    remove: $rootScope.server.dashboard+'/api/dashboard/remove/address/'
                };

                $scope.updateData = function() {
                    return {
                        _id: $scope.edit._id,
                        data: _.pick($scope.edit,'name','_street','_district','_town','house','housing')
                    }
                }

                $scope.$watch('town',function(newValue, oldValue){
                    if(newValue) {
                        if(!_.isEmpty(newValue)) {
                            if($scope.edit)
                                $scope.edit._town = newValue;
                            if(!$scope.new)
                                $scope.new = {};

                            $scope.new._town = newValue;

                            $scope.getAll();

                            $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/districts/'+newValue._id+'/',function(data){
                                $scope.districts = data;
                            });
                            $scope.getAll($rootScope.server.dashboard+'/api/dashboard/get/all/streets/'+newValue._id+'/',function(data){
                                $scope.streets = data;
                            });
                        }
                    }
                });
                $scope.$watch('street',function(newValue, oldValue){
                    if(newValue) {
                        $scope.getAll();
                    }
                });
                $scope.getAll();
            }]);
    }
);