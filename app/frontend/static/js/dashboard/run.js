define([
    './app',
    'ui'
], function (app,ui) {
    'use strict';

    return app.run(['cfpLoadingBar','$rootScope','$state','$stateParams','$templateCache','authorization','$http','$window','errorHandler.factory', function (cfpLoadingBar,$rootScope,$state,$stateParams,$templateCache,secure,$http,$window,$errorHandler) {

        $errorHandler.init();
        $rootScope.title = 'Турбо Агент';
        $rootScope.server = { search: 'http://localhost:3000', dashboard: 'http://localhost:3001'};
        $rootScope.hidePage = true;
        $rootScope.breadcrumbs = {};
        $rootScope.checkCurrentInPrimary = function(item) {
            if($rootScope.breadcrumbs.section && item)
                return $rootScope.breadcrumbs.section.state == item.state;
            else
                return false;
        }

        $rootScope.startLoadingBar = function() {
            cfpLoadingBar.start();
        };
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.completeLoadingBar = function () {
            cfpLoadingBar.complete();
        };

        $rootScope.$on("$stateChangeStart", function (event, next, nextParams, current, currentParams) {
            if(secure.isAuth()) {
                if(!secure.checkAccess(next)) {
                    $rootScope.$emit('ACCESS_DENIED');
                    event.preventDefault();
                }
            } else {
                loadAuth(function(){
                    if(!secure.checkAccess(next)) {
                        $rootScope.$emit('ACCESS_DENIED');
                        event.preventDefault();
                    }
                });
            }

            $rootScope.startLoadingBar();
            $templateCache.removeAll();
        });
        $rootScope.$on("$stateChangeSuccess", function (event, next, current) {
            $rootScope.completeLoadingBar();
        });
        $rootScope.$on("$stateChangeError", function (event, next, current) {
            $rootScope.completeLoadingBar();
        });
        $rootScope.$on('$viewContentLoaded',function(event) {
            ui.controls.init();
            ui.updateControls();
        });

        $rootScope.setAuthHeaders = function(data) {
            $http.defaults.headers.common["x-user-id"] = data.id;
            $http.defaults.headers.common["x-user-token"] = data.token;
        }

        function loadAuth(callback) {
            if($window.localStorage.u_t) {
                $http.post($rootScope.server.dashboard + '/api/load/token/',{token: $window.localStorage.u_t})
                    .success(function(res){
                        secure.doAuth(res);
                        callback();
                    })
                    .error(function(){
                        delete $window.localStorage.u_t;
                        $window.localStorage.returnUrl = '/dashboard/';
                        window.location.href = '/login/';
                    });
            } else {
                $window.localStorage.returnUrl = '/dashboard/';
                $window.location.href = '/login/';
            }
        }
    }]);
});