define([
    './app'
], function (app) {
    'use strict';

    return app.config(['$stateProvider','$urlRouterProvider','$httpProvider','$locationProvider', function ($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider) {

        $locationProvider.html5Mode(true);

        $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
        $httpProvider.defaults.transformRequest = function(data) {
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        };

        $stateProvider
            .state('dashboard', {
                url: '/dashboard/',
                templateUrl: '/templates/section/dashboard/index.html',
                controller: 'dashboard',
                access: {
                    users: '@'
                }
            })
            .state('settings',{
                url:'/dashboard/settings/',
                views: {
                    "": {
                        templateUrl: '/templates/section/dashboard/settings.html',
                        controller: 'settings'
                    },
                    "secondary-nav": {
                        templateUrl: '/templates/section/dashboard/settings/navigation.html'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.search_queries', {
                url:'search/requests/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/search/queries.html',
                        controller: 'settings.requests.queries'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.search_classifier', {
                url:'search/requests/classifier/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/search/classifier.html',
                        controller: 'settings.requests.classifier'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.geo_countries', {
                url:'geo/countries/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/geo/countries.html',
                        controller: 'settings.geo_countries'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.geo_areas', {
                url:'geo/areas/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/geo/areas.html',
                        controller: 'settings.geo_areas'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.geo_towns', {
                url:'geo/towns/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/geo/towns.html',
                        controller: 'settings.geo_towns'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.geo_districts', {
                url:'geo/districts/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/geo/districts.html',
                        controller: 'settings.geo_districts'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.geo_streets', {
                url:'geo/streets/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/geo/streets.html',
                        controller: 'settings.geo_streets'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('settings.geo_addresses', {
                url:'geo/addresses/',
                views: {
                    'settings.content': {
                        templateUrl: '/templates/section/dashboard/settings/geo/addresses.html',
                        controller: 'settings.geo_addresses'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('statistic',{
                url:'/dashboard/statistic/',
                views: {
                    "": {
                        templateUrl: '/templates/section/dashboard/statistic.html',
                        controller: 'statistic'
                    },
                    "secondary-nav": {
                        templateUrl: '/templates/section/dashboard/statistic/navigation.html'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('users',{
                url:'/dashboard/users/',
                views: {
                    "": {
                        templateUrl: '/templates/section/dashboard/users.html',
                        controller: 'users'
                    },
                    "secondary-nav": {
                        templateUrl: '/templates/section/dashboard/users/navigation.html'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('users.add',{
                url:'add/',
                views: {
                    'users.content': {
                        templateUrl: '/templates/section/dashboard/users/add.html',
                        controller: 'users.add'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('adverts',{
                url:'/dashboard/adverts/',
                views: {
                    "": {
                        templateUrl: '/templates/section/dashboard/adverts.html',
                        controller: 'adverts'
                    },
                    "secondary-nav": {
                        templateUrl: '/templates/section/dashboard/adverts/navigation.html'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('adverts.new',{
                url:'new/',
                views: {
                    'adverts.content': {
                        templateUrl: '/templates/section/dashboard/adverts/form.html',
                        controller: 'adverts.new'
                    }
                },
                access: {
                    roles: 'administrator'
                }
            })
            .state('error.404',{
                url:'/error/404/',
                templateUrl: '/templates/error/404.html',
                controller: function($rootScope) {
                    $rootScope.page_name = '/ Страница не найдена / Ошибка 404';
                }
            });

        $urlRouterProvider
            .when('/dashboard/','dashboard')
            .otherwise('/dashboard/error/404/');


    }]);
});