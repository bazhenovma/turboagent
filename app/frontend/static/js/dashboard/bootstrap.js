define([
    'angular',
    'ui',
    'jquery',
    'app',
    'run',
    'config'
], function (ng, ui, $) {
    'use strict';

    $(window).ready(function(){
        ui.app.init();
        ng.bootstrap(document, ['app']);
    })
});