define('ui',[
    'jquery',
    '/js/ui/app.js',
    '/js/ui/controls.js'
], function ($,app,controls) {
    'use strict';
    return {
        app: app,
        controls: controls,
        updateControls: function() {
            var self = this;

            setTimeout(function(){
                self.controls.init();
                self.controls.updateSelectors();
            },100);
        },
        _logTimer: (new Date()).getTime(),
        debugLog: function(msg){
            try {
                var t = '[' + (((new Date()).getTime() - this._logTimer) / 1000) + '] ';
                if (window.console && console.log) {
                    var args = Array.prototype.slice.call(arguments);
                    args.unshift(t);
                    //if (browser.msie || browser.mobile) {
                        //console.log(args.join(' '));
                    //} else {
                        console.log.apply(console, args);
                    //}
                }
            } catch(e) {
                console.error(e);
            }
        }
    };
});