define([
    'jquery',
    './controls.js'
],
function ($,uiControls) {
    'use strict';

    var self = {
        pinnFooter: function() {
            var documentHeight = $(document).height();
            var windowHeight = $(window).height();

            if(windowHeight >= documentHeight)
                $('.b-content').css('height',windowHeight-373+'px');
            else
                $('.b-content').css('height','');
        },
        documentActions: function() {
            $(window).resize(function(){
                self.pinnFooter();
            })
        }
    }

    return {
        init: function() {
            uiControls.init();
            //self.searchFormUIActions();
            self.documentActions();
            self.pinnFooter();
        }
    }
});