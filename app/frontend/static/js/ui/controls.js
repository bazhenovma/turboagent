define([
    'jquery'
],
    function ($) {
        'use strict';

    var self = {
        enableSelectorUI: function() {
            $(".b-form-field_type_select").chosen({disable_search_threshold: 10,width: '100%'});
        },
        bindButtonClick: function() {
            $('.b-button input').on('change',function(e){
                var active_class = 'b-button_state_active';

                if($(this).attr('type') == 'radio') {
                    $(this).parent().parent().parent().find('.b-button').removeClass(active_class);
                }
                this.checked ? $(this).parent().parent().addClass(active_class) : $(this).parent().parent().removeClass(active_class);
            });
        },
        calcForHint: function() {
            var hintBlocks = $('.b-hint-inside');

            $.each(hintBlocks,function(index,block){
                var down = $(block).hasClass('b-hint-inside_direction_up');
                var blockHint = $(block).find('.b-form-field-hint');

                if(blockHint.length == 0)
                    return;

                var contentLength = $(blockHint).find('.b-form-field-hint__content').text().length;
                if(contentLength > 25 && contentLength < 40) {
                    $(blockHint).css({
                        fontSize: 14+'px'
                    });
                }
                if(contentLength > 40) {
                    $(blockHint).css({
                        fontSize: 13+'px'
                    });
                }
                var widthHint = contentLength*7;
                if(widthHint < 100)
                    widthHint = 100;
                else {
                    if(widthHint > 200) {
                        widthHint = 200;
                    }
                }
                    $(blockHint).css({
                    width: widthHint+'px',
                    marginLeft: -widthHint/2-15+'px',
                    marginTop: !down ? '8px':0,
                    left:50+'%'
                });

                if(down) {
                    $(blockHint).find('.b-icon').addClass('b-icon_type_arrow-down');
                    $(blockHint).css({
                        top:0,
                        marginTop: -($(blockHint).height()+20)+'px'
                    });
                }
            });
        },
        addAnimateToRow: function(fieldModel,title) {
            var field = $('[ng-model="'+fieldModel+'"]');
            var parent = $(field).parent();
            $(field).attr('disabled','true');
            $(parent).addClass('i-field-in-progress');
            $(parent).append('<i class="b-icon b-icon_type_progress" title="'+title+'"></i>');
        },
        removeAnimateToRow: function(fieldModel) {
            var field = $('[ng-model="'+fieldModel+'"]');
            var parent = $(field).parent();
            $(field).removeAttr('disabled');
            $(parent).removeClass('i-field-in-progress');
            $(parent).find('.b-icon_type_progress').detach();
        }
    }

    return {
        addAnimateToRow: self.addAnimateToRow,
        removeAnimateToRow: self.removeAnimateToRow,
        updateSelectors: function() {
            $(".b-form-field_type_select").trigger("chosen:updated");
        },
        init: function() {
            self.enableSelectorUI();
            self.bindButtonClick();
            self.calcForHint();
        }

    }

});